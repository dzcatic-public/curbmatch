<?php

// Enqueue child theme style.css
add_action( 'wp_enqueue_scripts', function() {
    wp_enqueue_style( 'child-style', get_stylesheet_uri() );

    if ( is_rtl() ) {
    	wp_enqueue_style( 'mylisting-rtl', get_template_directory_uri() . '/rtl.css', [], wp_get_theme()->get('Version') );
    }
}, 500 );

// Happy Coding :)


function wpb_custom_new_menu() {
  register_nav_menu('my-custom-menu',__( 'Footer Col 1' ));
}
add_action( 'init', 'wpb_custom_new_menu' );


// Generate claim on submit 
// 
add_action( 'mylisting/submission/done', 'mg_claim_request' );
add_action( 'mylisting/submission/save-listing-data', 'mg_claim_request', 99 );
function mg_claim_request( $listing_id ) {
	$claimed = get_post_meta( $listing_id, '_claim-listing', true ); 
	foreach($claimed as $claim){
		if ( $claim== 'yes' ) \MyListing\Src\Claims\Claims::create( [
			'listing_id'      => $listing_id,
			'user_package_id' => get_post_meta( $listing_id, '_user_package_id', true ),
		] );
	}
	
}

//Reduce upload image size to 2MB
function ml_max_image_size( $file ) {
  $size = $file['size'];
  $size = $size / 1024;
  $type = $file['type'];
  $is_image = strpos( $type, 'image' ) !== false;
  $limit = 2048;
  $limit_output = '2MB';
  if ( $is_image && $size > $limit ) {
    $file['error'] = 'Image files must be smaller than ' . $limit_output;
  }//end if
  return $file;
}//end ml_max_image_size()

add_filter( 'wp_handle_upload_prefilter', 'ml_max_image_size' );


// ELEMENTOR - DISABLE GOOGLE FONTS
add_filter( 'elementor/frontend/print_google_fonts', '__return_false' );

// MYLISTING - DISABLE GOOGLE FONTS
add_action( 'wp_enqueue_scripts', function() {wp_dequeue_style( 'c27-google-fonts' );}, 10 );

/**

 * Bypass logout confirmation.

 */


function iconic_bypass_logout_confirmation() {
 
    global $wp;
 
    if ( isset( $wp->query_vars['customer-logout'] ) ) {
        wp_redirect( str_replace( '&amp;', '&', wp_logout_url( wc_get_page_permalink( '/' ) ) ) );
        exit;
    }

}  
// First, change the required password strength
add_filter( 'woocommerce_min_password_strength', 'reduce_min_strength_password_requirement' );
function reduce_min_strength_password_requirement( $strength ) {
    // 3 => Strong (default) | 2 => Medium | 1 => Weak | 0 => Very Weak (anything).
    return 0; 
}

// Second, change the wording of the password hint.
 
add_filter( 'password_hint', 'smarter_password_hint' );
function smarter_password_hint ( $hint ) {
    $hint = 'Hint: The password should be at least 8. characters long. To make it stronger, use upper and lower case letters, numbers, and symbols like ! " ? $ % ^ & ).).';
    return $hint;
}

add_filter( 'woocommerce_checkout_fields' , 'wpb_custom_additional_info' );
// Change placeholder text in Additional Notes
function wpb_custom_additional_info( $fields ) {
     $fields['order']['order_comments']['placeholder'] = '';
     $fields['order']['order_comments']['label'] = '';
     return $fields;
}
 
function wpbeginner_remove_version() {
    return '';
}
add_filter('the_generator', 'wpbeginner_remove_version');

function my_custom_login_logo() {
    echo '<style type="text/css">
    h1 a {background-image:url(https://preprod.curbmatch.com/wp-content/uploads/2020/11/logo-curbatch-v2.png) !important; margin:0 auto;}
    </style>';
}
add_filter( 'login_head', 'my_custom_login_logo' );