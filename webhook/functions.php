<?php

/*
*  Check if user is subscribed
*/
function is_subscribed($email, $apikey, $listid) {
    $userid = md5($email);
    $auth = base64_encode( 'user:'. $apikey );
    $data = array(
        'apikey'        => $apikey,
        'email_address' => $email
        );
    $json_data = json_encode($data);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://'. substr($apikey,strpos($apikey,'-')+1) .'.api.mailchimp.com/3.0/lists/'.$listid.'/members/' . $userid);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
        'Authorization: Basic '. $auth));
    curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
    $result = curl_exec($ch); 
    $json = json_decode($result);
    return $json->{'status'};
}
 
is_subscribed('dzenad@catic.com', '17c6b497c733266347b30b088431437b-us14', '646f65fdd6');

/*
* Update tag
*/
function updateTag($email, $status, $apikey,  $listid, $tag1){
	
	$email = md5( strtolower( $email ) );  
	$auth = base64_encode( 'user:'. $apikey );
	$data = array(
		'apikey'        => $apikey,
		'email_address' => $email, 
		'status' =>  $status,
		'tags'	        => array( array( 'name' => $tag1, 'status' => 'active' ))
							//array( array( 'name' => $tag1, 'status' => 'active' ),array( 'name' => $tag2, 'status' => 'active' ),array( 'name' => $tag3, 'status' => 'active' ), )
	);
	$json_data = json_encode($data);

	$url = 'https://'. substr($apikey,strpos($apikey,'-')+1) .'.api.mailchimp.com/3.0/lists/'.$listid.'/members/' . $email . '/tags';


	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
        'Authorization: Basic '. $auth));
	curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	 
	curl_setopt($ch, CURLOPT_POST, true);   
	curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
 
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec($ch);
	var_dump($result);
}
function updateTag2($email, $apikey, $listid, $tag1, $tag2){
	
	$email = md5( strtolower( $email ) );  
	$auth = base64_encode( 'user:'. $apikey );
	$data = array(
		'apikey'        => $apikey,
		'email_address' => $email,  
		'tags'	        => array(    
		array(    
		  'name' => $tag1,    
		  'status' => 'active'     
		),    
		array(    
		  'name' => $tag2,    
		  'status' => 'active'     
		)    
	  )    
	);
		
	$json_data = json_encode($data);

	$url = 'https://'. substr($apikey,strpos($apikey,'-')+1) .'.api.mailchimp.com/3.0/lists/'.$listid.'/members/' . $email . '/tags';


	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
        'Authorization: Basic '. $auth));
	curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	 
	curl_setopt($ch, CURLOPT_POST, true);   
	curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
 
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec($ch);
	var_dump($result);
}

//updateTag('dz@dzc.com', '17c6b497c733266347b30b088431437b-us14', '646f65fdd6', 'Samsung');

/*
* Subscribe user if didnt check type
*/ 
function subscribe( $email, $status, $listid, $apikey, $merge_fields = array('FNAME' => '','LNAME' => '') ){
    $data = array(
        'apikey'        => $apikey,
        'email_address' => $email, 
        'status'        => $status, 
        'merge_fields'  => $merge_fields
    );
    $mch_api = curl_init(); // initialize cURL connection
 
    curl_setopt($mch_api, CURLOPT_URL, 'https://' . substr($apikey,strpos($apikey,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $listid . '/members/' . md5(strtolower($data['email_address'])));
    curl_setopt($mch_api, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.base64_encode( 'user:'.$apikey )));
    curl_setopt($mch_api, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($mch_api, CURLOPT_RETURNTRANSFER, true); // return the API response
    curl_setopt($mch_api, CURLOPT_CUSTOMREQUEST, 'PUT'); // method PUT
    curl_setopt($mch_api, CURLOPT_TIMEOUT, 10);
    curl_setopt($mch_api, CURLOPT_POST, true);
    curl_setopt($mch_api, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($mch_api, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json
 
    $result = curl_exec($mch_api);
    return $result;
}

function subscribe_contact_us($email, $status, $listid, $apikey, $merge_fields = array('FNAME' => '','LNAME' => '') ){
    $data = array(
        'apikey'        => $apikey,
        'email_address' => $email, 
        'status'        => $status, 
	'tags'  => array('contact us'),
        'merge_fields'  => $merge_fields
    );
    $mch_api = curl_init(); // initialize cURL connection
 
    curl_setopt($mch_api, CURLOPT_URL, 'https://' . substr($apikey,strpos($apikey,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $listid . '/members/' . md5(strtolower($data['email_address'])));
    curl_setopt($mch_api, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.base64_encode( 'user:'.$apikey )));
    curl_setopt($mch_api, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($mch_api, CURLOPT_RETURNTRANSFER, true); // return the API response
    curl_setopt($mch_api, CURLOPT_CUSTOMREQUEST, 'PUT'); // method PUT
    curl_setopt($mch_api, CURLOPT_TIMEOUT, 10);
    curl_setopt($mch_api, CURLOPT_POST, true);
    curl_setopt($mch_api, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($mch_api, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json
 
    $result = curl_exec($mch_api);
    return $result;
}

function subscribe_landing_page($email, $status, $listid, $apikey, $merge_fields = array('FNAME' => '','LNAME' => '') ){
    $data = array(
        'apikey'        => $apikey,
        'email_address' => $email, 
        'status'        => $status, 
		'tags'  => array('partner lead'),
        'merge_fields'  => $merge_fields
    );
    $mch_api = curl_init(); // initialize cURL connection
 
    curl_setopt($mch_api, CURLOPT_URL, 'https://' . substr($apikey,strpos($apikey,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $listid . '/members/' . md5(strtolower($data['email_address'])));
    curl_setopt($mch_api, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.base64_encode( 'user:'.$apikey )));
    curl_setopt($mch_api, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($mch_api, CURLOPT_RETURNTRANSFER, true); // return the API response
    curl_setopt($mch_api, CURLOPT_CUSTOMREQUEST, 'PUT'); // method PUT
    curl_setopt($mch_api, CURLOPT_TIMEOUT, 10);
    curl_setopt($mch_api, CURLOPT_POST, true);
    curl_setopt($mch_api, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($mch_api, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json
 
    $result = curl_exec($mch_api);
    return $result;
}

function subscribe_landing_page_community($email, $status, $listid, $apikey, $merge_fields = array('FNAME' => '','LNAME' => '') ){
    $data = array(
        'apikey'        => $apikey,
        'email_address' => $email, 
        'status'        => $status, 
	'tags'  => array('community'),
        'merge_fields'  => $merge_fields
    );
    $mch_api = curl_init(); // initialize cURL connection
 
    curl_setopt($mch_api, CURLOPT_URL, 'https://' . substr($apikey,strpos($apikey,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $listid . '/members/' . md5(strtolower($data['email_address'])));
    curl_setopt($mch_api, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.base64_encode( 'user:'.$apikey )));
    curl_setopt($mch_api, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($mch_api, CURLOPT_RETURNTRANSFER, true); // return the API response
    curl_setopt($mch_api, CURLOPT_CUSTOMREQUEST, 'PUT'); // method PUT
    curl_setopt($mch_api, CURLOPT_TIMEOUT, 10);
    curl_setopt($mch_api, CURLOPT_POST, true);
    curl_setopt($mch_api, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($mch_api, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json
 
    $result = curl_exec($mch_api);
    return $result;
}
/*
* Create newsletter
*/ 
function subscribe_newsletter( $email, $status, $listid, $apikey, $merge_fields = array('FNAME' => '','LNAME' => '') ){
    $data = array(
        'apikey'        => $apikey,
        'email_address' => $email, 
		'tags'  => array('Newsletter'),
		//'tags'	        => array( array( 'name' => 'Newsletter', 'status' => 'active' )),
        'status'        => $status, 
        'merge_fields'  => $merge_fields
    );
    $mch_api = curl_init(); // initialize cURL connection
 
    curl_setopt($mch_api, CURLOPT_URL, 'https://' . substr($apikey,strpos($apikey,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $listid . '/members/' . md5(strtolower($data['email_address'])));
    curl_setopt($mch_api, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.base64_encode( 'user:'.$apikey )));
    curl_setopt($mch_api, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($mch_api, CURLOPT_RETURNTRANSFER, true); // return the API response
    curl_setopt($mch_api, CURLOPT_CUSTOMREQUEST, 'PUT'); // method PUT
    curl_setopt($mch_api, CURLOPT_TIMEOUT, 10);
    curl_setopt($mch_api, CURLOPT_POST, true);
    curl_setopt($mch_api, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($mch_api, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json
 
    $result = curl_exec($mch_api);
    return $result;
}

function subscribe_both( $email, $status, $listid, $apikey, $merge_fields = array('FNAME' => '','LNAME' => '') ){
    $data = array(
        'apikey'        => $apikey,
        'email_address' => $email, 
		'tags'  => array('Newsletter', 'Community'), 
        'status'        => $status, 
        'merge_fields'  => $merge_fields
    );
    $mch_api = curl_init(); // initialize cURL connection
 
    curl_setopt($mch_api, CURLOPT_URL, 'https://' . substr($apikey,strpos($apikey,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $listid . '/members/' . md5(strtolower($data['email_address'])));
    curl_setopt($mch_api, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.base64_encode( 'user:'.$apikey )));
    curl_setopt($mch_api, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($mch_api, CURLOPT_RETURNTRANSFER, true); // return the API response
    curl_setopt($mch_api, CURLOPT_CUSTOMREQUEST, 'PUT'); // method PUT
    curl_setopt($mch_api, CURLOPT_TIMEOUT, 10);
    curl_setopt($mch_api, CURLOPT_POST, true);
    curl_setopt($mch_api, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($mch_api, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json
 
    $result = curl_exec($mch_api);
    return $result;
}
/*
* Create community
*/ 
function subscribe_community( $email, $status, $listid, $apikey, $merge_fields = array('FNAME' => '','LNAME' => '') ){
    $data = array(
        'apikey'        => $apikey,
        'email_address' => $email, 
		'tags'  => array('CBSN community'),
		//'tags'	        => array( array( 'name' => 'Community', 'status' => 'active' )),
        'status'        => $status, 
        'merge_fields'  => $merge_fields
    );
    $mch_api = curl_init(); // initialize cURL connection
 
    curl_setopt($mch_api, CURLOPT_URL, 'https://' . substr($apikey,strpos($apikey,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $listid . '/members/' . md5(strtolower($data['email_address'])));
    curl_setopt($mch_api, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.base64_encode( 'user:'.$apikey )));
    curl_setopt($mch_api, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($mch_api, CURLOPT_RETURNTRANSFER, true); // return the API response
    curl_setopt($mch_api, CURLOPT_CUSTOMREQUEST, 'PUT'); // method PUT
    curl_setopt($mch_api, CURLOPT_TIMEOUT, 10);
    curl_setopt($mch_api, CURLOPT_POST, true);
    curl_setopt($mch_api, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($mch_api, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json
 
    $result = curl_exec($mch_api);
    return $result;
} 
 
?>
 